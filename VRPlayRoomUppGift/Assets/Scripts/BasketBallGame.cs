﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BasketBallGame : MonoBehaviour
{
    public GameObject _scoreTextGameObject;
    private TextMeshProUGUI _scoreTextMesh;
    private int _score;
    private bool _scoreFull;
        private void Start()
    {
        _score = 0;
        _scoreTextMesh = _scoreTextGameObject.GetComponent<TextMeshProUGUI>();
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Rigidbody>())
        {
        _score++;

            if (_score == 99 && !_scoreFull)
            {
                _scoreFull = true;

            }

            if (_score >= 10 && !_scoreFull)
            {
                _scoreTextGameObject.GetComponent<TMPro.TextMeshPro>().text = _score.ToString();
            }

            if (_score < 10 && !_scoreFull)
            {
            _scoreTextGameObject.GetComponent<TMPro.TextMeshPro>().text = "0" + _score.ToString();
            }   
        }
    }
}
