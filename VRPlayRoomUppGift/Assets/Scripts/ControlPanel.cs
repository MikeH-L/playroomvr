﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlPanel : MonoBehaviour
{

    public enum SPAWNOBJECTSA
    {
        CUBE,
        SPHERE,
        CYLINDER,
        RANDOM
    }

    public SPAWNOBJECTSA _spawnObjects;

    public GameObject _laserBeamSpawnGameObject;
    public GameObject[] _prefabsObjects;

    private float _fadeAlphaPerSec = 0.001f;
    private bool _spawningObject;


    private void Update()
    {

        if (Input.GetKeyDown(KeyCode.Space) && !_spawningObject)
        {
            _spawningObject = true;
            StartCoroutine(SpawnObject(_prefabsObjects[2]));
        }
    }



    public void SpawnObject(SPAWNOBJECTSA objectShape)
    {
        _spawningObject = true;
        if (SPAWNOBJECTSA.CUBE == objectShape)
        {
        StartCoroutine(SpawnObject(_prefabsObjects[0]));
        }


        if (SPAWNOBJECTSA.SPHERE == objectShape)
        {
            StartCoroutine(SpawnObject(_prefabsObjects[1]));
        }


        if (SPAWNOBJECTSA.CYLINDER == objectShape)
        {
            StartCoroutine(SpawnObject(_prefabsObjects[2]));
        }
    }



    IEnumerator FadeInAlphaCoroutine(Renderer alpha, Color FinshedColor)
    {
        float counter = 0f;
        float fadeTime = 4f;

        _laserBeamSpawnGameObject.SetActive(true);

        while (counter < fadeTime)
        {
            if (alpha.material.color.a <= 1)
            {
            alpha.material.color += new Color(0, 0, 0, _fadeAlphaPerSec);
            }

            counter += Time.deltaTime;
        yield return 0f;
        }
        alpha.material.color = FinshedColor;
    }



    IEnumerator SpawnObject(GameObject go)
    {
        GameObject _newSpawnObject = Instantiate(go, transform.position, transform.rotation);
        Renderer _newSpawnObjectRenderer = _newSpawnObject.GetComponent<Renderer>();
        _newSpawnObjectRenderer.material.color = Random.ColorHSV();

        Color colorFinished = _newSpawnObjectRenderer.material.color;
        Color AlphaColor = new Color(0f, 0f, 0f, 1f);

        _newSpawnObjectRenderer.material.color -= AlphaColor;
        StartCoroutine(FadeInAlphaCoroutine(_newSpawnObjectRenderer, colorFinished));

        yield return new WaitForSeconds(4f);
        _newSpawnObject.transform.parent = null;
        _newSpawnObject.GetComponent<Rigidbody>().useGravity = true;
        _newSpawnObject.GetComponent<Rigidbody>().isKinematic = false;
        _laserBeamSpawnGameObject.SetActive(false);
        yield return 0f;
        _spawningObject = false;
    }
}
