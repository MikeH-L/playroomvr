﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireScript : MonoBehaviour
{
    public Rigidbody _bulletRigidbody;

    public AudioSource _audioSourceBulletSOUNDFX;
    public ParticleSystem _gunFireParticleSystem;
    public float _bulletSpeed = 500f;
    public float _bulletLifeTime = 5f;
    private bool _hasNoGun;
    private bool _hasFired;
    private float counter;
    public float _coolDown = 1f;
    

    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Mouse0) && !_hasFired && !_hasNoGun)
        {
            _audioSourceBulletSOUNDFX.Play();
            _audioSourceBulletSOUNDFX.pitch = Random.Range(0.8f, 1.2f);
            _gunFireParticleSystem.Play();

            counter = 0f;
            _hasFired = true;

            Rigidbody newSpawnedBullet = Instantiate(_bulletRigidbody, transform);
            newSpawnedBullet.transform.parent = null;
            newSpawnedBullet.AddRelativeForce(0f, 0f, _bulletSpeed * Time.deltaTime, ForceMode.Impulse);
            Destroy(newSpawnedBullet.gameObject, _bulletLifeTime);
        }


        if (counter >= _coolDown)
        {
            _hasFired = false;
        }

        counter += Time.deltaTime;
    }
}
