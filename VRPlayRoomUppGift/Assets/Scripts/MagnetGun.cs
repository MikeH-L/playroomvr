﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagnetGun : MonoBehaviour
{
    public float _magnetPower = 5f;
    public float _magnetDistance = 10f;

    public Camera _mainCamera;
    public ParticleSystem _laserBeamMagnetParticleSystem;
    public ParticleSystem _spreadParticleSystem;
    public AudioSource _laserBeamAudioSource;
    private float _lengthOfSoundClip;
    private bool _ifSoundPLayed;


    private void Start()
    {
        _lengthOfSoundClip = _laserBeamAudioSource.clip.length;
    }




    void FixedUpdate()
    {

        if (Input.GetKeyDown(KeyCode.T))
        {
            Debug.Log("I Hit T Key");

                gameObject.GetComponent<Rigidbody>().isKinematic = false;
                gameObject.GetComponent<Rigidbody>().useGravity = true;
            gameObject.transform.parent = null;
        }


        if (Input.GetKey(KeyCode.Mouse0))
        {
            if (!_ifSoundPLayed)
            {
                _laserBeamAudioSource.Play();
                _ifSoundPLayed = true;
                StartCoroutine(WaitSoundCoroutine());
            }


            _laserBeamMagnetParticleSystem.Play();
            _spreadParticleSystem.Play();
            RaycastHit raycastHit;

            if (Physics.Raycast(_mainCamera.transform.position, _mainCamera.transform.forward, out raycastHit, _magnetDistance))
            {
                if (raycastHit.rigidbody != null)
                {
                    Debug.Log("Found Rigidbody");
                    Vector3 directionToSucker = transform.position - raycastHit.transform.position;
                    raycastHit.transform.GetComponent<Rigidbody>().AddForce(directionToSucker.normalized * _magnetPower, ForceMode.Impulse);
                }
            }
        }
    }

    IEnumerator WaitSoundCoroutine()
    {
        yield return new WaitForSeconds(_lengthOfSoundClip);
        _ifSoundPLayed = false;
        yield return 0f;
    }
}
