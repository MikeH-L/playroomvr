﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(PlayerMotor))]
public class PlayerController : MonoBehaviour
{
    
    [SerializeField]
    private float _speed = 5f;

    [SerializeField]
    private float _lookSensitivity = 4f;


    private PlayerMotor _motor;
    private bool _moving;
    public float _sprint = 10f;
    public float _jumpForce = 10f;
    private float _noJumpForce = 0f;
    private float _startSpeed;
    // Start is called before the first frame update
    void Start()
    {
        _motor = GetComponent<PlayerMotor>();
        Cursor.visible = false;
        _startSpeed = _speed;
    }

    // Update is called once per frame
    void Update()
    {
        float xMov = Input.GetAxisRaw("Horizontal");
        float zMov = Input.GetAxisRaw("Vertical");


        Vector3 moveHorizontal = transform.right * xMov;
        Vector3 moveVertical = transform.forward * zMov;


        Vector3 velocity = (moveHorizontal + moveVertical).normalized * _speed;


        //Apply movement
        _motor.Move(velocity);

        if (Input.GetKey(KeyCode.LeftShift))
        {
            _speed = _sprint;
        }

        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            _speed = _startSpeed;
        }

        //if (Input.GetKeyDown(KeyCode.Space))
        //{
        //    _motor.Jump(_jumpForce);
        //}
        //if (Input.GetKeyUp(KeyCode.Space))
        //{
        //    _motor.Jump(_noJumpForce);
        //}
        //Calucate rotation as a 3D Vector: (turning around)

        float yRot = Input.GetAxisRaw("Mouse X");

        Vector3 rotation = new Vector3(0f, yRot * _lookSensitivity, 0f);

        // Apply rotation

        _motor.Rotate(rotation);

        float xRot = Input.GetAxisRaw("Mouse Y");

        Vector3 cameraRotation = new Vector3(xRot * _lookSensitivity, 0f, 0f);

        // Apply rotation

        _motor.RotateCamera(cameraRotation);


        




    }


    void Sprint()
    {




    }





}
