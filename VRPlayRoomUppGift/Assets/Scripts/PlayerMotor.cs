﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Rigidbody))]
public class PlayerMotor : MonoBehaviour
{
    [SerializeField]
    private Camera cam;

    private Vector3 _velocity = Vector3.zero;
    private Vector3 _rotation = Vector3.zero;
    private Vector3 _cameraRotation = Vector3.zero;



    private Rigidbody rb;





    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }


    public void Move(Vector3 velocity)
    {
        _velocity = velocity;
    }
    
    public void Rotate(Vector3 rotiation)
    {
        _rotation = rotiation;
    }

    public void RotateCamera(Vector3 cameraRotation)
    {
        _cameraRotation = cameraRotation;
    }


    private void FixedUpdate()
    {
        PerformMovement();
        PerformRotation();
    }


    private void PerformMovement()
    {
        if (_velocity != Vector3.zero)
        {
            rb.MovePosition(rb.position + _velocity * Time.fixedDeltaTime);
        }
    }
     
    public void Jump(float _jumpForce)
    {
        rb.AddForce(0f, _jumpForce * Time.deltaTime, 0f, ForceMode.Impulse);
    }

    private void PerformRotation()
    {

        rb.MoveRotation(rb.rotation * Quaternion.Euler(_rotation));
        if (cam != null)

        {
            cam.transform.Rotate(-_cameraRotation);
        }
    }
}
